Talks
=====

This repository contains some talks I have given at Berlin FSFE group meetings.

Some of them (the HTML) are made with [deck.js](https://github.com/imakewebthings/deck.js) hence this is included in the repository as well. Including also a FSFE inspired theme for the slides of a deck.

Content
-------

Fediverse Fachtag Köln

 * [2023-09-22 Fediverse Fachtag Köln: Das Fediverse](https://git.fsfe.org/tobiasd/talks/src/branch/master/FediverseFachtag_Koeln_2023_DasFediverse.pdf) Aufzeichnung: [media.fsfe.org](https://media.fsfe.org/w/mKvxWdFrzEEbpjfzxBx7Rc)
 
Berliner Fediverse Tag

 * [2024-09-14: Das Fediverse in Europa](https://git.fsfe.org/tobiasd/talks/src/branch/master/Berliner_Fediverse_Tag_2024-Fediverse_in_Europa.pdf)
 
FOSDEM

 * [2025: Friendica - Under the radar since 2010](https://git.fsfe.org/tobiasd/talks/src/branch/master/20240201-FOSDEM-Friendica.under.the.radar.since.2010-en.pdf)

FSFE Supporter Meetings

 * [2016-05-12 FSFE Berlin: The Federation](https://git.fsfe.org/tobiasd/talks/src/branch/master/FSFE_Berlin_20160512_The_Federation.pdf)
 * [2017-11-09 FSFE Berlin: Boxes of Freedom](https://git.fsfe.org/tobiasd/talks/src/branch/master/FSFE_Berlin_20171109_Boxes_of_Freedom.pdf)
 * [2018-07-26 FSFE Berlin: Anonymity Matters](https://git.fsfe.org/tobiasd/talks/src/branch/master/FSFE_Berlin_20180726_Tor.pdf)
 * [2019-01-10 FSFE Berlin: Sichere Passwörter und deren Management (Folien)](https://git.fsfe.org/tobiasd/talks/src/branch/master/FSFE_Berlin_20190110_Sichere_Passwoerter_Slides.pdf)
 * [2019-01-10 FSFE Berlin: Sichere Passwörter und deren Management (Handout)](https://git.fsfe.org/tobiasd/talks/src/branch/master/FSFE_Berlin_20190110_Sichere_Passwoerter_Handout.pdf)
 * [2019-04-11 FSFE Berlin: Pragmatische Paketierung mit FPM (Folien)](https://git.fsfe.org/tobiasd/talks/src/branch/master/FSFE_Berlin_20190411_FPM_Slides.pdf)
 * [2019-04-11 FSFE Berlin: Pragmatische Paketierung mit FPM (Handout)](https://git.fsfe.org/tobiasd/talks/src/branch/master/FSFE_Berlin_20190411_FPM_Handout.pdf)
 * [2019-08-10 Schätzchen Berlin: Das Fediverse Freie Soziale Netzwerke - Alternativen zu Facebook, Instagram und nebenan.de (Folien)](https://git.fsfe.org/tobiasd/talks/src/branch/master/Schaetzchen_Berlin_20190810_Fediverse_Slides.pdf)
 * [2019-08-10 Schätzchen Berlin: Das Fediverse Freie Soziale Netzwerke - Alternativen zu Facebook, Instagram und nebenan.de (Handout)](https://git.fsfe.org/tobiasd/talks/src/branch/master/Schaetzchen_Berlin_20190810_Fediverse_Handout.pdf)

FSFE Assembly

 * [33c3 FSFE Assembly: The Federation](https://git.fsfe.org/tobiasd/talks/src/branch/master/FSFE_33c3-The-Federation.pdf)
 * [34c3 FSFE Assembly: Social Networking powered by FLOSS](https://git.fsfe.org/tobiasd/talks/src/branch/master/FSFE_34c3-Social_Networking_powered_by_FLOSS.pdf)
 * [37c3 Bits & Bäume Assembly: Running an NGO on Free Software](https://git.fsfe.org/tobiasd/talks/src/branch/master/37c3_TobiasDiekershoff_Running_an_NGO_on_FreeSoftware.pdf)

LCOY24

 * [about:Fediverse](https://git.fsfe.org/tobiasd/talks/src/branch/master/20241026-tobiasd-LCOY24-aboutFediverse.de.pdf)

SFSCON

 * [2024: about:Fediverse](https://git.fsfe.org/tobiasd/talks/branch/master/20241026-tobiasd-SFSCon24-aboutFediverse.en.pdf) [Recording](https://media.fsfe.org/w/g2fVwrmu3RGjtrixkwtwGQ)

Software Freedom Day

 * [2018: Einführung in AsciiDoc(tor)](https://git.fsfe.org/tobiasd/talks/src/branch/master/sfd2018)
 * [2019: Über Nachhaltigkeit Freier Software](https://git.fsfe.org/tobiasd/talks/raw/branch/master/SFD2019_Nachhaltigkeit_von_FLOSS.pdf)
 * [2024: How to run an NGO on Free Software](https://git.fsfe.org/tobiasd/talks/raw/branch/master/SFD2024_NGO_on_FLOSS.pdf)

License
-------

Unless otherwise noted my talks are licensed under a [Creative Commons CC-BY](https://creativecommons.org/licenses/by/4.0/) license.

deck.js is licensed under the MIT license (see the MIT-license.txt file).
