# Vortrag: Einführung in AsciiDoc(tor)

AsciiDic(tor) als Alternative zu Word, Libreoffe Writer, aber auch Markdown und LaTeX.

Vortrag ursprünglich gehalten auf dem Software Freedom Day 2018 in Berlin.

Ein Experiment für diesen Vortrag.
Die Folien für die Präsentation, die Speaker-Notes sowie das Handout als PDF, HTML und EPUB aus einem Guss und aus einer asciidoc Quelle.
Auch um zu sehen, wo es Probleme gibt.

Für die die eine Einführung in AsciiDoc(tor) suchen, nehmt nicht die Slides, sondern das Handout im PDF Format.
Darum ist das auch gerendert hier im Repository.

## Folien?

Die *Folien* für die Präsentation des Vortrages sind mit dem [reveal-js](https://revealjs.com/#/) Backend von AsciiDoctor realisiert, welches **nicht** Teil dieses Repositories ist.
Eine PDF Version der Folien kann in der Datei `slides.pdf` gefunden werden.
Die PDF Version enhält allerdings die Speaker-Notes nicht.

Anschließend `make slides` und ihr bekommt Folien und Speaker-Notes.

## Autor

Tobias Diekershoff.

## Share?

Na klar! Unter den Bedingungen der [CC-BY](https://creativecommons.org/licenses/by/4.0/) Lizenz.
